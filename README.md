# .NetCore + Aurelia + Typescript + DeveloperForce.Force (Salesforce)

This document will describe the process of setting up a .netCore project with WebAPI+Aurelia+DeveloperForce.Force.

### -1. General Whining
The setup is super simple, there is no extra anything.  There will be a number of these examples, and the whole point is that... examples.

### 0. Prerequirements
We are assuming you have Visual Studio 2015 Update 3, and .NetCore 2

Command prompt is now your friend, since you missed the 80s. I am using Cmder

And Lastly, we're going to be installing these globally.

1. Node - https://nodejs.org/en/download/ 
2. NPM - Node comes with some archaic version of NPM, you'll want to update that. Terminal Window - npm install -g npm
3. JSPM - npm install jspm -g
4. Typescript - npm install typescript -g
5. Typings - npm install typings -g

### 1. Start a new project.
Start a new project in Visual Studio 2015 selecting 'ASP.NET Core Web Application (.NET Core)'.  I selected "Web API".

### 2. Init JSPM
You need a tooling system to install javascript packages, JSPM to the rescue.

In terminal window, find the directory your project is in.  (\CodeLivesHere\Aurelia-WebAPI-DotNetCore\src\Aurelia-WebAPI-DotNetCore) and run
'jspm init' -- you will take all the defaults except the baseURL (which is wwwroot).

```
#!cmd
C:\CodeLivesHere\Aurelia-WebAPI-DotNetCore\src\Aurelia-WebAPI-DotNetCore>jspm init
warn Running jspm globally, it is advisable to locally install jspm via npm install jspm --save-dev.

Package.json file does not exist, create it? [yes]:
Would you like jspm to prefix the jspm package.json properties under jspm? [yes]:
Enter server baseURL (public folder path) [./]:wwwroot
Enter jspm packages folder [wwwroot\jspm_packages]:
Enter config file path [wwwroot\config.js]:
Configuration file wwwroot\config.js doesn't exist, create it? [yes]:
Enter client baseURL (public folder URL) [/]:
Do you wish to use a transpiler? [yes]:
Which ES6 transpiler would you like to use, Babel, TypeScript or Traceur? [babel]:
```

### 3. Install Aurelia Packages
From the same directory
```
jspm install aurelia-framework aurelia-bootstrapper
```

### 4. Create Entry Point
Create index.html file in wwwroot.  This will invoke the SystemJS module loader.
```
<!DOCTYPE html>
<html>
<head>
    <title>Aurelia-WebAPI-DotNetCore</title>
</head>
<body aurelia-app>
    <h1>Loading...</h1>

    <script src="jspm_packages/system.js"></script>
    <script src="config.js"></script>
    <script>
      System.import('aurelia-bootstrapper');
    </script>
</body>
</html>
```

### 5. Add Aurelia Files
By default, Aurelia Boostrap loader (aurelia-bootstrapper) expects the view model / view pair of files, named app.  (Also in wwwroot)
##### App.js
```
#!javascript
export class App {
    constructor() {
        this.message = "";
    }
    activate() {
        this.message = 'hello world';
    }   
}
```
##### App.html
```
<template>
  ${message}
</template>
```

### 6.  Add Static File Ability
Edit Startup.cs by adding a 'Use Static Files' call.
```
#!c#
public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
  {
   loggerFactory.AddConsole(Configuration.GetSection("Logging"));
   loggerFactory.AddDebug();
   app.UseStaticFiles();
   app.UseMvc();
  }
```
After adding this, you should get a red line, click Add Potential Fixes, and then include the StaticFiles reference.


### 7.  Build and Run
Ctrl-F5 should run and load http://localhost:[port]/api/values, which is the default controller for the api.  So you will manually need to go to http://localhost:[port]/index.html

You should get a screen that says 'hello world'.

### 8. The end.